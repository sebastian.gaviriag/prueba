/*Given an array of integers nums and an integer target, return indices of the two numbers such that they
add up to target.

You may assume that each input would have exactly one solution, 
and you may not use the same element twice.

You can return the answer in any order.

Example:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].



 * Note: The returned array must be malloced, assume caller calls free().
int* twoSum(int* nums, int numsSize, int target, int* returnSize){

}*/

#include <stdio.h>
#include <stdlib.h>
//#include <mem.h>

int* twoSum(int* array, int arraySize, int target/*, int* returnSize*/);

int* twoSum(int* array, int arraySize, int target/*, int* returnSize*/) {

    int* returnArray = calloc(2, sizeof(int));
    //int* returnArray = (int*)malloc(sizeof(int) * 2);

    for (int i = 0; i < --arraySize; i++)
    {
        for (int j = ++i; j < arraySize; j++)
        {
            if (array[i] + array[i] == target)
            {
                continue;
            }
            else if (array[j] + array[j] == target)
            {
                continue;
            }
            else if (array[i] + array[j] == target)
            {
                returnArray[0] = i;
                returnArray[1] = j;
            }
        }
    }
    
    return returnArray;
}

int main() {

    int* array;
    int arraySize, target;
    
    printf("\nIngrese una longitud deseada para el array: ");
    scanf("%d", &arraySize);
    
    array = calloc(arraySize, sizeof(int));
    //array = (int*) malloc(sizeof(int) * arraySize);

    printf("\nIngrese %d numeros enteros\n", arraySize);
    
    for (int i = 0; i < arraySize; i++)
    {
        printf("\narray[%d]: ", i);
        scanf("%d", &array[i]);
    }

    printf("\nEl arreglo resultante es: ");
    for (int j = 0; j < arraySize; j++)
    {
        if (j == 0)
        {
            printf("[%d, ", array[j]);
        }
        else if ( j != 0 && j < arraySize - 1)
        {
            printf("%d, ", array[j]);
        }
        else
        {
            printf("%d]", array[j]);
        }
        
    }
    printf("\n\n");

    printf("\nIngrese un numero entero objetivo: ");
    scanf("%d", &target);

    int* returnArray = twoSum(array, arraySize, target);

    for (int k = 0; k < 2; k++)
    {
        printf("%d ", returnArray[k]);
    }
    
    free(returnArray);
    free(array);
    return 0;
}